require 'rails_helper'

RSpec.describe Tweet, type: :model do
  context "validations" do
    let(:tweet) { create(:tweet, body: "Hello world!") }
    let(:invalid_tweet) { build(:tweet, body: Faker::Lorem.characters(number: 190)) }

    it "validates the length of the body" do
      expect(tweet).to be_valid
    end

    it "validates the length of the body" do
      invalid_tweet.save

      expect(invalid_tweet.errors.messages[:body]).to include("is too long (maximum is 180 characters)")
    end
  end
end
