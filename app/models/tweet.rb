class Tweet < ApplicationRecord
  belongs_to :user

  validates_length_of :body, maximum: 180

  validate :tweet_time, on: :create

  private

  def tweet_time
    past_tweet = Tweet.where(user_id: self.user_id, body: self.body, created_at: 24.hours.ago..Time.now)

    errors.add(:body, "You can't create the same tweet twice in the same day.") if past_tweet.present?
  end
end
